# FMM & Constant pH GROMACS

![logo of FMM and constant pH code](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/images/FMM_ConstantPH_t.png)
This repository contains the source code for a GROMACS version that incorporates the Fast Multipole Method (FMM) electrostatic solver and associated constant pH MD simulation code.

For documentation, installation instructions, and usage guidelines, please visit [our documentation page](https://grubmueller.pages.mpcdf.de/docs-gromacs-fmm-constantph/).

## Repository branches

The source of the current stable version of the constant pH code is found in the branch `constant_ph_stable`.

## Authors

This project was developed by the [Theoretical and Computational Biophysics group](https://www.mpinat.mpg.de/grubmueller) led by Prof. Dr. Grübmueller at the Max Planck Institute for Multidisciplinary Sciences (MPI-NAT).
